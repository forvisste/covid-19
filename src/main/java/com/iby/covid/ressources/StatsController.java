package com.iby.covid.ressources;

import java.io.IOException;
import java.net.URISyntaxException;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.iby.covid.dto.MyMoroccoStats;

import com.iby.covid.services.CoronaVirusDataService;

@RestController
public class StatsController {
	@Autowired
	public CoronaVirusDataService service1;
	
	@CrossOrigin(origins = "*")
	@GetMapping("/stat")
	public MyMoroccoStats fetchData() throws URISyntaxException, IOException {

		return service1.fetchVirusData();
	}

}
