package com.iby.covid.ressources;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


import com.iby.covid.dto.SeriesDtoOut;
import com.iby.covid.services.CallApiService;

@RestController
public class CovidController {

	@Autowired
	public CallApiService service;

	@GetMapping("/covid19")
	public List<SeriesDtoOut> getData() throws URISyntaxException {

		return service.callApi();
	}
	
}
