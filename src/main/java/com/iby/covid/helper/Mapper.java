package com.iby.covid.helper;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.iby.covid.dto.MoroccoStats;
import com.iby.covid.dto.MyMoroccoStats;
import com.iby.covid.dto.MyStats;
import com.iby.covid.dto.OutApiCovid;
import com.iby.covid.dto.SeriesDtoOut;

@Service
public class Mapper {

	public SeriesDtoOut mapOutApiCovToSeriesDtoOut(OutApiCovid outApiCovid) {
		SeriesDtoOut response = new SeriesDtoOut();
		response.setCases(outApiCovid.getCases());
		LocalDate day = LocalDateTime.parse(outApiCovid.getDate().replace("Z", "")).toLocalDate();
		response.setDate(day.toString());
		return response;
	}

	public List<SeriesDtoOut> mapOutApiCovToSeriesDtoOut(List<OutApiCovid> list) {

		List<SeriesDtoOut> reponse = new ArrayList<>();
		list.forEach(outApiCovid -> {
			reponse.add(mapOutApiCovToSeriesDtoOut(outApiCovid));
		});
		return reponse;

	}

	public MyStats mapMoroccoStatsToMyStats(MoroccoStats moroccoStats) {
		MyStats resp = new MyStats();
		resp.setLatestTotalCases(moroccoStats.getConfirmed());
		LocalDate day = LocalDateTime.parse(moroccoStats.getDate().replace("Z", "")).toLocalDate();
		resp.setDate(day.toString());
		resp.setDeaths(moroccoStats.getDeaths());
		resp.setRecovered(moroccoStats.getRecovered());

		return resp;
	}

	public List<MyStats> mapMoroccoStatsToMyStats(List<MoroccoStats> statsList) {
		List<MyStats> respone = new ArrayList<>();
		statsList.forEach(moroccoStats -> {
			respone.add(mapMoroccoStatsToMyStats(moroccoStats));
		});
		return respone;
	}

	public MyMoroccoStats mapMoroccoStatsToMyMoroccoStats(List<MoroccoStats> statsLists) {
		MyMoroccoStats myStats = new MyMoroccoStats();
		List<String> date = new ArrayList<>();
		List<Integer> deaths = new ArrayList<>();
		List<Integer> latestTotalCases = new ArrayList<>();
		List<Integer> recovered = new ArrayList<>();

		statsLists.forEach(mystats -> {
			date.add(mapMoroccoStatsToMyStats(mystats).getDate());
			deaths.add(mapMoroccoStatsToMyStats(mystats).getDeaths());
			latestTotalCases.add(mapMoroccoStatsToMyStats(mystats).getLatestTotalCases());
			recovered.add(mapMoroccoStatsToMyStats(mystats).getRecovered());
		});

		myStats.setDate(date);
		myStats.setDeaths(deaths);
		myStats.setLatestTotalCases(latestTotalCases);
		myStats.setRecovered(recovered);

		return myStats;

	}
}
