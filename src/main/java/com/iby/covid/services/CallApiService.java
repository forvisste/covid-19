package com.iby.covid.services;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.iby.covid.dto.OutApiCovid;
import com.iby.covid.dto.SeriesDtoOut;
import com.iby.covid.helper.Mapper;

@Service
public class CallApiService {

	@Autowired
	RestTemplate restTemplate;
	@Autowired
	Mapper mapper;

	@Value("${covid.api.url}")
	public String url;

	public List<SeriesDtoOut> callApi() throws URISyntaxException {
		
		List<OutApiCovid> retour;

		URI uri = new URI(url);
		ResponseEntity<OutApiCovid[]> response =restTemplate.getForEntity(uri, OutApiCovid[].class);
		retour =Arrays.asList(response.getBody());
		return mapper.mapOutApiCovToSeriesDtoOut(retour);

	}

}
