package com.iby.covid.services;

import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.iby.covid.dto.MoroccoStats;
import com.iby.covid.dto.MyMoroccoStats;
import com.iby.covid.dto.MyStats;
import com.iby.covid.helper.Mapper;

@Service
public class CoronaVirusDataService {
	@Autowired
	Mapper mapper;
	@Autowired
	RestTemplate restTemplate;

	@Value("${location.stats}")
	public String url1;

	private List<MyStats> allStats = new ArrayList<>();

	public List<MyStats> getAllStats() {
		return allStats;
	}

	public List<MyStats> callCovidApi() throws URISyntaxException {

		List<MoroccoStats> retour;

		URI uri = new URI(url1);
		ResponseEntity<MoroccoStats[]> response = restTemplate.getForEntity(uri, MoroccoStats[].class);
		retour = Arrays.asList(response.getBody());
		return mapper.mapMoroccoStatsToMyStats(retour);
	}

	@PostConstruct
	public MyMoroccoStats fetchVirusData() throws URISyntaxException, IOException {

		List<MoroccoStats> retour;

		URI uri = new URI(url1);
		ResponseEntity<MoroccoStats[]> response = restTemplate.getForEntity(uri, MoroccoStats[].class);
		retour = Arrays.asList(response.getBody());
		
		return mapper.mapMoroccoStatsToMyMoroccoStats(retour);
	}

	

}
