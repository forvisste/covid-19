package com.iby.covid.dto;

import java.util.List;

public class MyMoroccoStats {
	private List<String> date;

	private List<Integer> deaths;

	private List<Integer> latestTotalCases;

	private List<Integer> recovered;

	private List<Integer> diffFromPrevDay;

	public MyMoroccoStats() {

	}

	public MyMoroccoStats(List<String> date, List<Integer> deaths, List<Integer> latestTotalCases,
			List<Integer> recovered, List<Integer> diffFromPrevDay) {
		super();
		this.date = date;
		this.deaths = deaths;
		this.latestTotalCases = latestTotalCases;
		this.recovered = recovered;
		this.diffFromPrevDay = diffFromPrevDay;
	}

	public List<String> getDate() {
		return date;
	}

	public void setDate(List<String> date) {
		this.date = date;
	}

	public List<Integer> getDeaths() {
		return deaths;
	}

	public void setDeaths(List<Integer> deaths) {
		this.deaths = deaths;
	}

	public List<Integer> getLatestTotalCases() {
		return latestTotalCases;
	}

	public void setLatestTotalCases(List<Integer> latestTotalCases) {
		this.latestTotalCases = latestTotalCases;
	}

	public List<Integer> getRecovered() {
		return recovered;
	}

	public void setRecovered(List<Integer> recovered) {
		this.recovered = recovered;
	}

	public List<Integer> getDiffFromPrevDay() {
		return diffFromPrevDay;
	}

	public void setDiffFromPrevDay(List<Integer> diffFromPrevDay) {
		this.diffFromPrevDay = diffFromPrevDay;
	}

	@Override
	public String toString() {
		return "MyMoroccoStats [date=" + date + ", deaths=" + deaths + ", latestTotalCases=" + latestTotalCases
				+ ", recovered=" + recovered + ", diffFromPrevDay=" + diffFromPrevDay + "]";
	}
	

}
