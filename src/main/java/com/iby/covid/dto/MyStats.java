package com.iby.covid.dto;

public class MyStats {
	
	private String date;

	private int deaths;

	private int latestTotalCases;

	private int recovered;

	private int diffFromPrevDay;

	public MyStats() {
		super();
	}

	public MyStats(String date, int deaths, int latestTotalCases, int recovered, int diffFromPrevDay) {
		super();
		this.date = date;
		this.deaths = deaths;
		this.latestTotalCases = latestTotalCases;
		this.recovered = recovered;
		this.diffFromPrevDay = diffFromPrevDay;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getDeaths() {
		return deaths;
	}

	public void setDeaths(int deaths) {
		this.deaths = deaths;
	}

	public int getLatestTotalCases() {
		return latestTotalCases;
	}

	public void setLatestTotalCases(int latestTotalCases) {
		this.latestTotalCases = latestTotalCases;
	}

	public int getRecovered() {
		return recovered;
	}

	public void setRecovered(int recovered) {
		this.recovered = recovered;
	}

	public int getDiffFromPrevDay() {
		return diffFromPrevDay;
	}

	public void setDiffFromPrevDay(int diffFromPrevDay) {
		this.diffFromPrevDay = diffFromPrevDay;
	}

	@Override
	public String toString() {
		return "MyStats [date=" + date + ", deaths=" + deaths + ", latestTotalCases="
				+ latestTotalCases + ", recovered=" + recovered + ", diffFromPrevDay=" + diffFromPrevDay + "]";
	}
	
	
}
