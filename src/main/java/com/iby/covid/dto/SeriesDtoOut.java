package com.iby.covid.dto;

import java.io.Serializable;

public class SeriesDtoOut implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3262581563101391086L;

	private int cases;
	private String date;

	public SeriesDtoOut() {
		super();
	}

	public SeriesDtoOut(int cases, String date) {
		super();
		this.cases = cases;
		this.date = date;
	}

	public int getCases() {
		return cases;
	}

	public void setCases(int cases) {
		this.cases = cases;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "SeriesDtoOut [cases=" + cases + ", date=" + date + "]";
	}

}
