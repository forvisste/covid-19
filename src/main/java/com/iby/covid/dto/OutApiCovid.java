package com.iby.covid.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OutApiCovid implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8813684988619997999L;
	@JsonProperty("Country")
	private String country;

	@JsonProperty("Date")
	private String date;

	@JsonProperty("Cases")
	private int cases;

	@JsonProperty("Status")
	private String status;

	public OutApiCovid() {
		super();
	}

	public OutApiCovid(String country, String date, int cases, String status) {
		super();
		this.country = country;
		this.date = date;
		this.cases = cases;
		this.status = status;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getCases() {
		return cases;
	}

	public void setCases(int cases) {
		this.cases = cases;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "OutApiCovid [country=" + country + ", date=" + date + ", cases=" + cases + ", status=" + status + "]";
	}

}
