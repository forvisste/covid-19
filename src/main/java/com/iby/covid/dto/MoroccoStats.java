package com.iby.covid.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MoroccoStats implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8429486245741615357L;

	@JsonProperty("Country")
	private String country;

	@JsonProperty("Date")
	private String date;

	@JsonProperty("Deaths")
	private int deaths;

	@JsonProperty("Confirmed")
	private int confirmed;

	@JsonProperty("Recovered")
	private int recovered;

	@JsonProperty("Active")
	private int active;
	
	private int diffFromPrevDay;

	public MoroccoStats() {
		super();
	}


	public MoroccoStats(String country, String date, int deaths, int confirmed, int recovered, int active,
			int diffFromPrevDay) {
		super();
		this.country = country;
		this.date = date;
		this.deaths = deaths;
		this.confirmed = confirmed;
		this.recovered = recovered;
		this.active = active;
		this.diffFromPrevDay = diffFromPrevDay;
	}



	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getDeaths() {
		return deaths;
	}

	public void setDeaths(int deaths) {
		this.deaths = deaths;
	}

	public int getConfirmed() {
		return confirmed;
	}

	public void setConfirmed(int confirmed) {
		this.confirmed = confirmed;
	}

	public int getRecovered() {
		return recovered;
	}

	public void setRecovered(int recovered) {
		this.recovered = recovered;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}
	

	public int getDiffFromPrevDay() {
		return diffFromPrevDay;
	}


	public void setDiffFromPrevDay(int diffFromPrevDay) {
		this.diffFromPrevDay = diffFromPrevDay;
	}


	@Override
	public String toString() {
		return "MoroccoStats [country=" + country + ", date=" + date + ", deaths=" + deaths + ", confirmed=" + confirmed
				+ ", recovered=" + recovered + ", active=" + active + ", diffFromPrevDay=" + diffFromPrevDay + "]";
	}


	

}
