package com.iby.covid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiCovidApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiCovidApplication.class, args);
	}

}
